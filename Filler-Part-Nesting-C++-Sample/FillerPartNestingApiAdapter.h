#ifndef FILLERPARTNESTINGSAMPLE_FILLERPARTNESTINGAPIADAPTER_H
#define FILLERPARTNESTINGSAMPLE_FILLERPARTNESTINGAPIADAPTER_H


#include <iostream>
#include <sstream>

/**
 * Contains a configuration for retrieving an access token via the NORMCUT authorization service based on Azure AD B2C.
 * Default values are configured for staging environment.
 */
class AuthorizationConfig {
public:
  /**
   * Base Url of the OpenID Connect authorization service
   */
  std::string authorizationServiceBaseUrl = "https://normcutb2cstaging.b2clogin.com";

  /**
   * Endpoint of ROPC authorization route
   */
  std::string authorizationServicePath = "/normcutb2cstaging.onmicrosoft.com/B2C_1A_ROPC_AUTH_CUTTER/oauth2/v2.0/token";

  /**
   * Grant type used for authorization
   */
  std::string grantType = "password";

  /**
   * API application Id
   */
  std::string clientId = "4411694e-df82-47da-83d0-89ac7d1c9774";

  /**
   * Scopes necessary for accessing the API
   */
  std::string scope = "openid%20https://normcutb2cstaging.onmicrosoft.com/4411694e-df82-47da-83d0-89ac7d1c9774/access_as_user%20offline_access";

  /**
   * Token types to be returned by authorization service
   */
  std::string responseType = "token%20id_token";

  /**
   * Builds a ROPC request body from given credentials as specified here:
   * https://docs.microsoft.com/en-us/azure/active-directory-b2c/add-ropc-policy?tabs=app-reg-ga&pivots=b2c-custom-policy#test-the-ropc-flow
   * @param username username of the to be authenticated user
   * @param pass password of the to be authenticated user
   * @return a ROPC request body following the application/x-www-form-urlencoded schema
   */
  std::string authorizationRequestBody(const std::string& username, const std::string& pass) const {
    std::stringstream bodyStream;
    bodyStream << "username=" << username << "&password=" << pass << "&grant_type=" << grantType << "&scope=" << scope
               << "&client_id=" << clientId << "&response_type=" << responseType;
    return bodyStream.str();
  }
};

/**
 * Provides an interface for executing filler part nesting functionality using the NORCMUT filler part nesting api. The
 * API has resource based authorization in place and a valid access token authenticating the accessing user is required for
 * every action. A token can be retrieved using getAccessToken.
 * @remark For demonstration purposes only.
 */
class FillerPartNestingApiAdapter {
public:
  FillerPartNestingApiAdapter() = delete;
  /**
   * Get a valid access token for a NORMCUT account enabling access to the NORMCUT filler part nesting API.
   * @param username username of the account
   * @param pass password of the account
   * @param config configuration of the authorization service
   * @return a valid access token
   */
  static std::string getAccessToken(const std::string &username, const std::string &pass, const AuthorizationConfig &config);

  /**
   * Creates a cutting plan resource for a given svg or dxf file of a nesting (should contain a sheet for best results)
   * @param fileName nesting file name
   * @param fileContent content of the nesting file
   * @param accessToken identifying the user
   * @return the id of the created cutting plan geometry
   */
  static std::string createCuttingPlanGeometry(const std::string &fileName, const std::string &fileContent,
                                               const std::string &accessToken);

  /**
   * Create a cutting plan for a given geometry id
   * @param cuttingPlanGeometryId the cutting plan geometry id the cutting plan should be created for
   * @param cuttingPlanName name of the cutting plan
   * @param accessToken identifying the user
   * @return the id of the created cutting plan
   */
  static std::string createCuttingPlan(const std::string &cuttingPlanGeometryId, const std::string &cuttingPlanName,
                                       const std::string &accessToken);

  /**
   * Creates a cutting plan optimization for a given cutting plan id
   * @param cuttingPlanId specifying the cutting plan that should be optimized
   * @param accessToken identifying the user
   * @return the id of the created cutting plan optimization
   */
  static std::string createCuttingPlanOptimization(const std::string &cuttingPlanId, const std::string &accessToken);

  /**
   * Gets the id of the cutting plan geometry that is resulting from an optimization. If the optimization has not been
   * finished yet, blocks and waits for its completion.
   * @param cuttingPlanOptimizationId specifying the cutting plan optimization
   * @param accessToken identifying the user
   * @return the id of the resulting cutting plan geometry containing filler parts
   */
  static std::string getAndWaitForOptimizedGeometryId(const std::string &cuttingPlanOptimizationId,
                                                      const std::string &accessToken);

  /**
   * Gets the geometry data of a cutting plan geometry.
   * @param geometryId specifying the cutting plan geometry
   * @param asSvg gets svg data if true, dxf otherwise
   * @param fillerPartsOnly gets only the geometry of filler parts if true, includes original parts geometry otherwise
   * @param pretty applies human readable styling if true
   * @param intrinsicSize uses realistic scale if true, otherwise geometry is scaled to fit viewbox
   * @param accessToken identifying the user
   * @return the geometry data
   */
  static std::string getOptimizedGeometry(const std::string &geometryId, bool asSvg, bool fillerPartsOnly, bool pretty,
                                   bool intrinsicSize, const std::string &accessToken);

private:
  static const std::string API_BASE_URL;
  static const std::string OPTIMIZATION_STATE_PENDING;
  static const std::string OPTIMIZATION_STATE_PROCESSING;
  static const std::string OPTIMIZATION_STATE_SUCCESS;
  static const std::string OPTIMIZATION_STATE_MISSING_DEMAND;
  static const std::string OPTIMIZATION_STATE_FAILED;

  static std::string getResource(const std::string &path, const std::string &accessToken);
  static std::string postResource(const std::string &path, const std::string& jsonBody, const std::string &accessToken);
  static std::tuple<std::string, std::string> getCuttingPlanOptimization(const std::string &cuttingPlanOptimizationId,
                                                                         const std::string &accessToken);
};

#endif // FILLERPARTNESTINGSAMPLE_FILLERPARTNESTINGAPIADAPTER_H
