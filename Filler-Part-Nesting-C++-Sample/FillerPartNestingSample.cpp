#include <iostream>
#include <stdexcept>

#include <boost/program_options.hpp>
#include <fstream>

#include "FillerPartNestingApiAdapter.h"

namespace po = boost::program_options;

/**
 * Uses ifstream to read content of a file and return it as string
 * @param filePath file to read
 * @return file content
 */
std::string loadFileAsString(std::string filePath) {
  std::ifstream t(filePath);
  if (!t.good()) {
    throw std::domain_error("File not found.");
  }
  std::stringstream buffer;
  buffer << t.rdbuf();
  return buffer.str();
}

int main(int argc, char * argv[]) {
  // Configure input via command line arguments
  po::options_description desc("Allowed options");
  desc.add_options()
      ("help", "produce help message")
      ("username", po::value<std::string>(), "set username for api access")
      ("password", po::value<std::string>(), "set password for api access")
      ("filePath", po::value<std::string>(), "dxf file that will be used for filler part nesting")
      ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  std::string username, password, filePath;
  if (vm.count("username")) {
    username = vm["username"].as<std::string>();
  } else {
    std::cout << "Username needs to be set.";
    return 1;
  }

  if (vm.count("password")) {
    password = vm["password"].as<std::string>();
  } else {
    std::cout << "Password needs to be set.";
    return 1;
  }

  if (vm.count("filePath")) {
    filePath = vm["filePath"].as<std::string>();
  } else {
    std::cout << "File Path needs to be set.";
    return 1;
  }

  // Return API access token from authorization service
  auto accessToken = FillerPartNestingApiAdapter::getAccessToken(username, password, AuthorizationConfig());

  std::cout << "Got Token: " << accessToken << std::endl;

  // Create cutting plan geometry resource by loading and posting dxf file
  std::string content = loadFileAsString(filePath);
  auto cuttingPlanGeometryId = FillerPartNestingApiAdapter::createCuttingPlanGeometry("testFile.dxf", content, accessToken);

  std::cout << "Successfully uploaded dxf file as cutting plan geometry with id " << cuttingPlanGeometryId << std::endl;

  // Create cutting plan resource by specifying id of previously created cutting plan geometry
  auto cuttingPlanId = FillerPartNestingApiAdapter::createCuttingPlan(cuttingPlanGeometryId, "test cutting plan", accessToken);

  std::cout << "Successfully created cutting plan for dxf file with id " << cuttingPlanId << std::endl;

  // Using the cutting plan id we can now create and trigger a cutting plan optimization which will start the actual
  // filler part nesting process
  auto cuttingPlanOptimizationId = FillerPartNestingApiAdapter::createCuttingPlanOptimization(cuttingPlanId, accessToken);

  std::cout << "Successfully triggered cutting plan optimization with id " << cuttingPlanOptimizationId << std::endl;

  // The API is implementing a polling pattern, for details see
  // https://docs.microsoft.com/en-us/azure/architecture/patterns/async-request-reply#solution
  auto optimizedGeometryId = FillerPartNestingApiAdapter::getAndWaitForOptimizedGeometryId(cuttingPlanOptimizationId, accessToken);

  std::cout << "Optimization finished, optimized geometry with id " << optimizedGeometryId << std::endl;

  // With the retrieved id referencing the resulting geometry we can get its data
  auto optimizedGeometryAsSvg = FillerPartNestingApiAdapter::getOptimizedGeometry(optimizedGeometryId, true, false, true, false, accessToken);

  std::cout << "Optimized cutting plan as svg string: " << optimizedGeometryAsSvg << std::endl;

  // Handle placement of filler parts, they share the original dxf document coordinate space...
  // e.g. for getting the geometry of filler parts only (as dxf):
  // auto fillerPartsAsDxf = getOptimizedGeometry(optimizedGeometryId, false, true, false, true, accessToken);
  // ...

  return 0;
}
