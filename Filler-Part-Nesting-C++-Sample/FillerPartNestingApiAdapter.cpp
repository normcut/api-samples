#define CPPHTTPLIB_OPENSSL_SUPPORT

#include "FillerPartNestingApiAdapter.h"

#include <iostream>
#include <stdexcept>

#include <httplib.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;


const std::string FillerPartNestingApiAdapter::API_BASE_URL = "https://staging.api.normcut.com";

const std::string FillerPartNestingApiAdapter::OPTIMIZATION_STATE_PENDING = "pending";
const std::string FillerPartNestingApiAdapter::OPTIMIZATION_STATE_PROCESSING = "processing";
const std::string FillerPartNestingApiAdapter::OPTIMIZATION_STATE_SUCCESS = "success";
const std::string FillerPartNestingApiAdapter::OPTIMIZATION_STATE_MISSING_DEMAND = "missing demand";
const std::string FillerPartNestingApiAdapter::OPTIMIZATION_STATE_FAILED = "failed";

/**
 * Abstracts basic http-lib usage and error handling for GET requests.
 */
std::string FillerPartNestingApiAdapter::getResource(const std::string &path, const std::string &accessToken) {
  httplib::Client cli(API_BASE_URL);
  cli.set_bearer_token_auth(accessToken.c_str());

  if (auto res = cli.Get(path.c_str())) {
    if (res->status == 200) {
      return res->body;
    } else {
      std::cout << "ERROR: Received non-success status code: " << res->status << " and error: " << res->body
                << std::endl;
      throw std::domain_error("ERROR");
    }
  } else {
    auto err = res.error();
    std::cout << "ERROR: " << err << std::endl;
    throw std::domain_error("ERROR");
  }
}

/**
 * Abstracts basic http-lib usage and error handling for POST requests.
 */
std::string FillerPartNestingApiAdapter::postResource(const std::string &path, const std::string& body, const std::string &accessToken) {
  httplib::Client cli(API_BASE_URL);
  cli.set_bearer_token_auth(accessToken.c_str());

  if (auto res = cli.Post(path.c_str(), body.c_str(), "application/json")) {
    if (res->status == 201) {
      auto responseObject = json::parse(res->body);
      return responseObject.get<std::string>();
    } else {
      std::cout << "ERROR: Received non-success status code: " << res->status << " and error: " << res->body
                << std::endl;
      return NULL;
    }
  } else {
    auto err = res.error();
    std::cout << "ERROR: " << err << std::endl;
    throw std::domain_error("ERROR");
  }
}

/**
 * Retrieves a valid access token for the NORMCUT api by running the ROPC endpoint of the authorization service.
 */
std::string FillerPartNestingApiAdapter::getAccessToken(const std::string &username, const std::string &pass, const AuthorizationConfig &config) {
  httplib::Client cli(config.authorizationServiceBaseUrl);

  std::string accessToken;
  if (auto res = cli.Post(config.authorizationServicePath.c_str(),
                          config.authorizationRequestBody(username, pass), "application/x-www-form-urlencoded")) {
    if (res->status == 200) {
      auto responseObject = json::parse(res->body);
      return responseObject["access_token"].get<std::string>();
    } else {
      std::cout << "ERROR: Received non-success status code: " << res->status << " and error: " << res->body
                << std::endl;
      return NULL;
    }
  } else {
    auto err = res.error();
    std::cout << "ERROR: " << err << std::endl;
    throw std::domain_error("ERROR");
  }
}

std::string FillerPartNestingApiAdapter::createCuttingPlanGeometry(const std::string &fileName,
                                                                   const std::string &fileContent,
                                                                   const std::string &accessToken) {
  httplib::Client cli(API_BASE_URL);
  cli.set_bearer_token_auth(accessToken.c_str());

  httplib::MultipartFormDataItems items = {
      {"data", fileContent, fileName, "application/dxf"},
      {"name", "test file", "", ""},
  };

  if (auto res = cli.Post("/CuttingPlanGeometry", items)) {
    if (res->status == 201) {
      auto responseObject = json::parse(res->body);
      return responseObject.get<std::string>();
    } else {
      std::cout << "ERROR: Received non-success status code: " << res->status << " and error: " << res->body
                << std::endl;
      return NULL;
    }
  } else {
    auto err = res.error();
    std::cout << "ERROR: " << err << std::endl;
    throw std::domain_error("ERROR");
  }
}

std::string FillerPartNestingApiAdapter::createCuttingPlan(const std::string &cuttingPlanGeometryId,
                                                           const std::string &cuttingPlanName,
                                                           const std::string &accessToken) {
  httplib::Client cli(API_BASE_URL);
  cli.set_bearer_token_auth(accessToken.c_str());

  json requestBody = {{"geometryId", cuttingPlanGeometryId}, {"name", cuttingPlanName}};
  return postResource("/CuttingPlan", requestBody.dump(), accessToken);
}

std::string FillerPartNestingApiAdapter::createCuttingPlanOptimization(const std::string &cuttingPlanId,
                                                                       const std::string &accessToken) {
  httplib::Client cli(API_BASE_URL);
  cli.set_bearer_token_auth(accessToken.c_str());

  json requestBody = {{"cuttingPlanId", cuttingPlanId}};
  return postResource("/CuttingPlanOptimization", requestBody.dump(), accessToken);
}

std::tuple<std::string, std::string>
FillerPartNestingApiAdapter::getCuttingPlanOptimization(const std::string &cuttingPlanOptimizationId,
                                                        const std::string &accessToken) {
  std::stringstream resourceUrlStream;
  resourceUrlStream << "/CuttingPlanOptimization/" << cuttingPlanOptimizationId;
  auto response = getResource(resourceUrlStream.str(), accessToken);
  auto responseObject = json::parse(response);
  auto state = responseObject["state"].get<std::string>();
  std::string optimizedGeometryId = responseObject.value("optimizedGeometryId", "");
  return std::tuple<std::string, std::string>(state, optimizedGeometryId);
}

/**
 * By implementing a blocking polling behavior this method queries the specified cutting plan optimization to retrieve
 * an optimized geometry id.
 */
std::string FillerPartNestingApiAdapter::getAndWaitForOptimizedGeometryId(const std::string &cuttingPlanOptimizationId,
                                                                          const std::string &accessToken) {
  int retryDelay = 500;
  // cutting plan optimization state description:
  // https://docs.normcut.com/api/filler-part-nesting#cutting-plan-optimization
  std::string state;
  std::string optimizedGeometryId;
  std::tie(state, optimizedGeometryId) = getCuttingPlanOptimization(cuttingPlanOptimizationId, accessToken);
  do {
    std::this_thread::sleep_for(std::chrono::milliseconds(retryDelay));
    std::tie(state, optimizedGeometryId) = getCuttingPlanOptimization(cuttingPlanOptimizationId, accessToken);
  } while (state == OPTIMIZATION_STATE_PENDING || state == OPTIMIZATION_STATE_PROCESSING);

  if (state == OPTIMIZATION_STATE_MISSING_DEMAND) {
    throw std::domain_error("Missing demand");
  } else if (state == OPTIMIZATION_STATE_FAILED) {
    throw std::domain_error("Optimization failed");
  }

  return optimizedGeometryId;
}

std::string FillerPartNestingApiAdapter::getOptimizedGeometry(const std::string &geometryId, bool asSvg, bool fillerPartsOnly,
                                                              bool pretty, bool intrinsicSize,
                                                              const std::string &accessToken) {
  std::string format = asSvg ? "svg" : "dxf";
  std::stringstream resourceUrlStream;
  resourceUrlStream << "/CuttingPlanGeometry/" << geometryId << "/data"
                    << "?format=" << format << "&includeParts=" << std::boolalpha << !fillerPartsOnly
                    << "&pretty=" << std::boolalpha << pretty << "&pretty=" << std::boolalpha << intrinsicSize;
  return getResource(resourceUrlStream.str(), accessToken);
}
