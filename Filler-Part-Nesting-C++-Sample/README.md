## Filler Part Nesting API C++ Sample
This sample is demonstrating the usage of the [NORMCUT filler part nesting API](https://docs.normcut.com/api/filler-part-nesting) within a simple C++11 console application. Functionality includes:
* authentication via authorization service 
* sending a dxf file containing a nesting
* receiving filler parts to improve material usage of the given nesting as svg or dxf

Libraries used:
* [cpp-httplib](https://github.com/yhirose/cpp-httplib)
* [nlohmann/json](https://github.com/nlohmann/json)
* [boost](https://www.boost.org)


### Setup
#### 1. Create a NORMCUT account
In order to use the API you will need to register with us to create a free account. This can be done [here](https://normcutb2cstaging.b2clogin.com/normcutb2cstaging.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1A_SIGNUP_SIGNIN_CUTTER&client_id=2ad62e49-009e-4499-a536-af9153a14d26&nonce=defaultNonce&redirect_uri=https%3A%2F%2Fnormcut.com%2F&scope=openid&response_type=code&prompt=login) and takes 1 minute. 
#### 2. Install Dependencies
For the application to work you need to have these dependencies installed and configured to allow cmake finding them.
* OpenSSL
* boost

#### 3. Build & Run
##### Build
Thanks to cmake building should be a simple matter of running `cmake .` & `cmake --build .`
##### Run
In order to run the sample application for a given dxf file execute:
`./bin/FillerPartNestingSample --username <user> --password <password> --filePath <filePath>`

You can give it a try using a file from the `testfile` directory.

```asm
username: your NORMCUT username
password: your NORMCUT password
filePath: a dxf file containing a nesting (should contain a rectangle marking the sheet for best results)
```